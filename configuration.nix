{
   config,
   pkgs,
   ...
}: {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

   boot.loader.systemd-boot.enable = true;
   boot.loader.efi.canTouchEfiVariables = true;
   # enable camera feed
   boot.extraModulePackages = with config.boot.kernelPackages; [
     v4l2loopback.out
   ];
   boot.kernelModules = [
     "v4l2loopback"
   ];
   boot.extraModprobeConfig = ''
     options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
   '';
   networking.hostName = "tsunami"; # Define your hostname.
   networking.networkmanager.enable = true;
   time.timeZone = "America/Phoenix";
   i18n.defaultLocale = "en_US.UTF-8";
   i18n.extraLocaleSettings = {
     LC_ADDRESS = "en_US.UTF-8";
     LC_IDENTIFICATION = "en_US.UTF-8";
     LC_MEASUREMENT = "en_US.UTF-8";
     LC_MONETARY = "en_US.UTF-8";
     LC_NAME = "en_US.UTF-8";
     LC_NUMERIC = "en_US.UTF-8";
     LC_PAPER = "en_US.UTF-8";
     LC_TELEPHONE = "en_US.UTF-8";
     LC_TIME = "en_US.UTF-8";
   };

   hardware = {
     bluetooth.enable = true;
     opengl.driSupport32Bit = true;
   };

   security = {
     rtkit.enable = true;
     sudo.enable = false;
     doas.enable = true;
     doas.extraRules = [{
       users = [ "enmesh" ];
       keepEnv = true;
       persist = true;
     }];
   };

   services = {
     blueman.enable = true;
     pipewire = {
       enable = true;
       alsa.enable = true;
       alsa.support32Bit = true;
       pulse.enable = true;
       jack.enable = true;
     };
   };

   users.users.enmesh = {
    isNormalUser = true;
    description = "enmesh";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [];
    shell = pkgs.zsh;
   };

   programs = {
     hyprland.enable = true;
     steam.enable = true;
   };
   system.stateVersion = "23.05";

   nix = {
      package = pkgs.nixVersions.stable;
      extraOptions = ''
         experimental-features = nix-command flakes
      '';
      gc = {
         automatic = true;
      };
      settings = {
         auto-optimise-store = true;
         trusted-users = ["enmesh"];
      };
   };

   nixpkgs.config = {
      allowUnfree = true;
      allowNonFree = true;
      allowUnsupportedSystem = true;
   };

   environment = {
     shells = [pkgs.bashInteractive pkgs.zsh];
     systemPackages = with pkgs; [
       wineWowPackages.stable
       wineWowPackages.waylandFull
     ];
   };

   fonts = {
      fontDir.enable = true;
      fonts = with pkgs; [
         fira-code
         ibm-plex
         mononoki
         roboto
         fira
      ];
   };

   programs.zsh.enable = true;
}
