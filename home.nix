{
   config,
   pkgs,
   lib,
   ...
}: {
   home = {
      username = "enmesh";
      homeDirectory = "/home/enmesh";
      enableNixpkgsReleaseCheck = true;
      packages = with pkgs; [
         # cli
         ## utils
         alacritty
         curl
         doas
         htop
         kakoune
         mosh
         openssh
         ripgrep

         ## audio
         ffmpeg_6-full

         ## video
         gphoto2
         imagemagick
         mpv
         v4l-utils

         # desktop
         ## apps
         discord
         firefox
         prismlauncher
         obs-studio

         ## utils
         pamixer
         sirula
      ];
      file = {
      };
      sessionPath = ["$HOME/bin"];
      sessionVariables = {
         LC_ALL = "en_US.UTF-8";
         LESSHISTFILE = "/dev/null";
         TZ = "America/Phoenix";
         VISUAL = "kak";
       };
       shellAliases = {
         ls = "ls -aFGhl";
         motd = "$VISUAL $HOME/documents/motd.txt";
         todo = "$VISUAL $HOME/TODO.md";
         vi = "$VISUAL";
       };
    stateVersion = "23.05";
  };
  programs = {
      alacritty = {
         enable = true;
         settings = {
            colors = {
               primary = {
                  background = "#1c2023";
                  foreground = "#c7ccd1";
               };
               normal = {
                  black = "#1c2023";
                  red = "#c7ae95";
                  green = "#95c7ae";
                  yellow = "#aec795";
                  blue = "#ae95c7";
                  magenta = "#c795ae";
                  cyan = "#95aec7";
                  white = "#c7ccd1";
               };
               bright = {
                  black = "#747c84";
                  red = "#c7ae95";
                  green = "#95c7ae";
                  yellow = "#aec795";
                  blue = "#ae95c7";
                  magenta = "#c795ae";
                  cyan = "#95aec7";
                  white = "#f3f4f5";
               };
            };
            window = {
               dimensions = {
                  columns = 96;
                  lines = 16;
               };
               padding = {
                  x = 10;
                  y = 10;
               };
            };
            font = {
               normal = {
                  family = "FiraCode";
               };
               size = 12;
            };
            cursor = {
               style = {
                  blinking = "On";
               };
            };
         };
      };
      zsh = {
         enable = true;
         initExtra = ''
           set -o emacs

           fmt -80 ~/Documents/motd.txt
           alias nixgrade="doas nixos-rebuild switch --flake ~/.config/home-manager/"
           alias streamup="gphoto2 --stdout --capture-movie | ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -f v4l2 /dev/video0"

           PS1='%1d %B%F{cyan}λ%f%F{magenta}=%f%b '
         '';
       };
   };
}

